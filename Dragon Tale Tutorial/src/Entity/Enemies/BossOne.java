package Entity.Enemies;

import java.awt.Graphics2D;

import Audio.AudioPlayer;
import Entity.*;
import TileMap.TileMap;

import java.awt.image.BufferedImage;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class BossOne extends Enemy {
	
	private BufferedImage[] sprites;
	
	public BossOne(TileMap tm){
		
		super(tm);
		
		moveSpeed = 0.8;
		maxSpeed = 0.8;
		fallSpeed = 0.2;
		maxFallSpeed = 10.0;
		
		width = 30;
		height = 30;
		cwidth = 20;
		cheight = 20;
		
		health = maxHealth = 2;
		damage = 1;
		
		// load sprites
		try{
			BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("/Sprites/Enemies/slugger.gif"));
			
			sprites = new BufferedImage[3];
			for(int i = 0; i < sprites.length; i++){
				sprites[i] = spritesheet.getSubimage(i*width,0,width,height);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		animation = new Animation();
		animation.setFrames(sprites);
		animation.setDelay(300);
		right = true;
		facingRight = true;
		
	}

}
