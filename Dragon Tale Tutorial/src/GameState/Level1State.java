package GameState;

import Main.GamePanel;
import TileMap.*;
import Entity.*;
import Entity.Enemies.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import Audio.AudioPlayer;


public class Level1State extends GameState {

	private TileMap tileMap;
	private Background bg;
	
	private Player player;
	
	private ArrayList<Enemy> enemies;
	private ArrayList<Explosion> explosions;
	
	private HUD hud;
	
	private AudioPlayer bgMusic;
	private HashMap<String, AudioPlayer> fx;
	private Graphics2D h;
	
	public Level1State(GameStateManager gsm){
		this.gsm = gsm;
		init();
	}
	
	public void init(){
		
		tileMap = new TileMap(30);
		tileMap.loadTiles("/Tilesets/grasstileset.gif");
		tileMap.loadMap("/Maps/level1-1.map");
		tileMap.setPosition(0, 0);
		tileMap.setTween(1);
		
		bg = new Background("/Backgrounds/grassbg1.gif", 0.1);
		
		player = new Player(tileMap);
		player.setPosition(100,100);
		
		populateEnemies();
		
		
		explosions = new ArrayList<Explosion>();
		
		hud = new HUD(player);
		
		bgMusic = new AudioPlayer("/Music/level1-1.mp3");
		bgMusic.play();
		
		fx = new HashMap<String, AudioPlayer>();
		fx.put("death", new AudioPlayer("/SFX/death2.mp3"));
		fx.put("testdeath", new AudioPlayer("/SFX/test.mp3"));
		
	}
	
	private void populateEnemies(){
		enemies = new ArrayList<Enemy>();
		
		Slugger s;
		Point[] points = new Point[]{
			new Point(250, 100),
			new Point(860, 200),
			new Point(1525, 200),
			new Point(1680, 200),
			new Point(1800, 200)
		};
		for(int i = 0; i < points.length; i++){
			s = new Slugger(tileMap);
			s.setPosition(points[i].x, points[i].y);
			enemies.add(s);
		}
	}
	
	public void update() {
		
		//update player
		player.update();
		if(player.isDead()){
			//player.remove();
			player.setPosition(100,600);
			bgMusic.stop();
		}
		tileMap.setPosition(GamePanel.WIDTH / 2 - player.getx(), GamePanel.HEIGHT / 2 - player.gety());
		
		//set background (if you want it to move with position)
		bg.setPosition(tileMap.getx(), tileMap.gety());
		
		//attack enemies
		player.checkAttack(enemies);
		
		
		//update all enemies
		for(int i = 0; i < enemies.size(); i++){
			Enemy e = enemies.get(i);
			e.update();
			if(e.isDead()){
				enemies.remove(i);
				i--;
				explosions.add(new Explosion(e.getx(), e.gety()));
				fx.get("testdeath").play();
			}
		}
		
		// update explosions
		for(int i = 0; i < explosions.size(); i++){
			explosions.get(i).update();
			if(explosions.get(i).shouldRemove()){
				explosions.remove(i);
				i--;
			}
		}
	}
	
	public void draw(Graphics2D g){
		//clear Screen
		//g.setColor(Color.WHITE);
		//g.fillRect(0,0, GamePanel.WIDTH,GamePanel.HEIGHT);
		
		//draw bg
		bg.draw(g);
		
		//draw tilemap
		tileMap.draw(g);
		
		//drawPlayer
		player.draw(g);
		if(player.isDead()){
			g.setColor(Color.WHITE);
			g.drawString("Game Over", GamePanel.WIDTH/2 - 30, GamePanel.HEIGHT/2);
			g.drawString("Press ENTER to Restart", GamePanel.WIDTH/2 - 30, GamePanel.HEIGHT/2 + 30);
		}
		
		//draw enemies
		for(int i = 0; i < enemies.size(); i++){
			enemies.get(i).draw(g);
		}
		
		//draw explosions
		for(int i = 0; i < explosions.size(); i++){
			explosions.get(i).setMapPosition((int)tileMap.getx(), (int)tileMap.gety());
			explosions.get(i).draw(g);
		}
		
		//draw hud
		hud.draw(g); 
		
	}
	public void keyPressed(int k){
		if(!player.isDead()){
			if(k == KeyEvent.VK_LEFT)	player.setLeft(true);
			if(k == KeyEvent.VK_RIGHT)	player.setRight(true);
			if(k == KeyEvent.VK_UP)		player.setUp(true);
			if(k == KeyEvent.VK_DOWN)	player.setDown(true);
			if(k == KeyEvent.VK_W)		player.setJumping(true);
			if(k == KeyEvent.VK_E)		player.setGliding(true);
			if(k == KeyEvent.VK_R)		player.setScratching();
			if(k == KeyEvent.VK_F)		player.setFiring();
			if(k == KeyEvent.VK_T)		player.setSuperSpeed(true);
		}else{
			if(k == KeyEvent.VK_ENTER) this.init();
		}
			
	}
	public void keyReleased(int k){
		if(!player.isDead()){
			if(k == KeyEvent.VK_LEFT)	player.setLeft(false);
			if(k == KeyEvent.VK_RIGHT)	player.setRight(false);
			if(k == KeyEvent.VK_UP)		player.setUp(false);
			if(k == KeyEvent.VK_DOWN)	player.setDown(false);
			if(k == KeyEvent.VK_W)		player.setJumping(false);
			if(k == KeyEvent.VK_E)		player.setGliding(false);
			if(k == KeyEvent.VK_T)		player.setSuperSpeed(false);
		}
	}
}
